import 'dart:convert';

import 'package:flutter_api/repository/local_preferences.dart';

import '../model/fruit_model.dart';

class StorageFruitRepository {
  StorageFruitRepository() {
    _storage = LocalPreferences();
    init();
  }

  late LocalPreferences _storage;

  late List<String>? _fruits;

  init() async {
    final fruits = await _storage.read('fruits');
    if (fruits == null) {
      await _storage.save('fruits', []);
      _fruits = [];
    } else {
      _fruits = fruits;
    }
  }

  Future<Fruit?> readByName(String fruitName) async {
    final fruit = _fruits!.firstWhere(
        (fruit) => Fruit.fromJsonString(fruit).name == fruitName,
        orElse: () => "");
    if (fruit.isEmpty) {
      return Future.error('Fruit not found');
    } else {
      return Fruit.fromJsonString(fruit);
    }
  }

  Future<int> create(String fruitName, Map<String, dynamic> data) async {
    final storedFruit = await readByName(fruitName).catchError((e) => null);
    if (storedFruit == null) {
      data['id'] = _fruits?.length;
      _fruits?.add(data.toString());
      await _storage.save("fruits", _fruits);
      return data['id']!;
    } else {
      return Future.error('Fruit already exists');
    }
  }

  Future<void> update(String fruitName, Map<String, dynamic> data) async {
    final storedFruit = await readByName(fruitName).catchError((e) => null);
    if (storedFruit != null) {
      if (data["createdBy"] != storedFruit.createdBy) {
        return Future.error('You can not update this fruit');
      } else {
        Map<String, dynamic> storedFruitMap = storedFruit.toJson();
        for (final key in data.keys) {
          storedFruitMap[key] = data[key] ?? storedFruitMap[key];
          if (key == "nutritions") {
            if (data[key] != null) {
              for (var keyNutrition in data[key].keys) {
                storedFruitMap[key][keyNutrition] = data[key][keyNutrition] ??
                    storedFruitMap[key][keyNutrition];
              }
            }
          }
        }
        final index = _fruits!.indexOf(storedFruit.toJsonString());
        _fruits![index] = storedFruitMap.toString();
        await _storage.save("fruits", _fruits);
      }
    }
  }

  Future<void> delete(String fruitName) async {
    final fruit = await readByName(fruitName);
    if (fruit != null) {
      _fruits!.remove(fruit.toJsonString());
      await _storage.save("fruits", _fruits);
    }
  }

  Future<List<Fruit>?> readAll() async{
    try{
      await init();
      return _fruits!.map<Fruit>((e) => Fruit.fromJsonString(e)).toList();
    }catch(e){
      return Future.error("Error reading fruits");
    }
  }
}
