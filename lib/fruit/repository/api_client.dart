import 'package:flutter_api/fruit/model/fruit_model.dart';
import 'package:http/http.dart' as http;
import 'dart:convert' as convert;

class FruitApiClient {
  final baseUrl = "https://www.fruityvice.com/api/fruit";

  Future<List<Fruit>> getFruitsFromApi(
      {int max = 100, String nutritions = "carbohydrates"}) async {
    var uri =
        Uri.parse("$baseUrl/$nutritions").resolveUri(Uri(queryParameters: {
      "max": max,
    }));

    try {
      final response = await http.get(uri).timeout(Duration(seconds: 3));
      if (response.statusCode == 200) {
        var jsonResponse = convert.jsonDecode(response.body);
        return jsonResponse
            .map<Fruit>(
                (dataFruit) => Fruit.fromJsonString(dataFruit.toString()))
            .toList();
      } else {
        return Future.error('Client error ${response.statusCode}');
      }
    } catch (e) {
      return Future.error('Client error Timeout');
    }
  }
}
