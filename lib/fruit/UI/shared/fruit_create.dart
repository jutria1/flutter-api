import 'package:flutter/material.dart';
import 'package:flutter_api/bloc/responsive/responsive_bloc.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import 'custom_fruit_form.dart';


class FruitCreateWidget extends StatelessWidget {
  /// Is a [StatelessWidget] that create or edit a fruit.
  ///
  /// This widget use [CustomFruitForm] to create or edit a fruit.
  /// Also, this widget has a title
  const FruitCreateWidget({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<ResponsiveBloc, ResponsiveState>(
      builder: (context, state) {
        final theme = state.themeData!;
        return Container(
          margin: const EdgeInsets.only(top: 20, left: 20, right: 20),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              Text(
                "Create new fruit",
                style: theme.textTheme.headline1,
              ),
              const SizedBox(height: 30),
              Expanded(
                child: ListView(
                  controller: ScrollController(),
                  scrollDirection: Axis.vertical,
                  children: [
                    CustomFruitForm(),
                  ],
                ),
              ),
            ],
          ),
        );
      },
    );
  }
}
