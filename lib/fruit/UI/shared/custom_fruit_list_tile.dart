import 'package:flutter/material.dart';
import 'package:flutter_api/bloc/responsive/responsive_bloc.dart';
import 'package:flutter_api/user/blocs/auth/login/login_bloc.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import '../../model/fruit_model.dart';

class CustomFruitListTile extends StatelessWidget {
  CustomFruitListTile({
    Key? key,
    required this.fruit,
    this.onTap,
    this.onEdit,
    this.onDelete,
  }) : super(key: key);

  final Fruit fruit;
  final VoidCallback? onTap;
  final VoidCallback? onEdit;
  final VoidCallback? onDelete;

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<ResponsiveBloc, ResponsiveState>(
        builder: (context, state) {
      final theme = state.themeData!;
      final userName = LoginBloc.getUser(context)!.name;
      return fruit.createdBy == userName
          ? ListTile(
              title: Text(fruit.name, style: theme.textTheme.headline3),
              subtitle: Text("Family: ${fruit.family}",
                  style: theme.textTheme.subtitle1),
              trailing: PopupMenuButton<Widget>(
                itemBuilder: (context) {
                  return [
                    PopupMenuItem(
                      onTap: onEdit,
                      value: const Text('Edit'),
                      child: Row(
                        children: const [
                          Icon(Icons.edit),
                          SizedBox(width: 3),
                          Text('Edit'),
                        ],
                      ),
                    ),
                    PopupMenuItem(
                      value: const Text('Delete'),
                      onTap: onDelete,
                      child: Row(
                        children: const [
                          Icon(Icons.delete),
                          SizedBox(width: 2),
                          Text("Delete")
                        ],
                      ),
                    ),
                  ];
                },
              ),
              onTap: onTap,
            )
          : ListTile(
              title: Text(fruit.name, style: theme.textTheme.headline3),
              subtitle: Text("Family: ${fruit.family}",
                  style: theme.textTheme.subtitle1),
              onTap: onTap,
            );
    });
  }
}
