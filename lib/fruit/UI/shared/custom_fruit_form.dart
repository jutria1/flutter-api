import 'package:flutter/material.dart';
import 'package:flutter_api/bloc/responsive/responsive_bloc.dart';
import 'package:flutter_api/fruit/bloc/crud_fruit/crud_fruit_bloc.dart';
import 'package:flutter_api/user/blocs/auth/login/login_bloc.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import '../../../UI/shared/custom_buttom.dart';
import '../../../UI/shared/custom_snackbar.dart';
import '../../../UI/shared/custom_text_field.dart';
import '../../../config/validations.dart';
import '../../model/fruit_model.dart';

class CustomFruitForm extends StatelessWidget {
  /// Is a basic form to create or edit a fruit.
  ///
  /// It uses the [fruit] to fill the form.
  /// It uses the [_controller] to create or edit a fruit.
  /// [CustomFruitForm] constains all text fields to create or edit a fruit.
  CustomFruitForm({
    Key? key,
    this.fruit,
  }) : super(key: key);
  final _formKey = GlobalKey<FormState>();
  final _genusController = TextEditingController();
  final _familyController = TextEditingController();
  final _nameController = TextEditingController();
  final _carbohydratesController = TextEditingController();
  final _proteinController = TextEditingController();
  final _fatController = TextEditingController();
  final _caloriesController = TextEditingController();
  final _sugarController = TextEditingController();
  Fruit? fruit;

  /// Update text controllers with [fruit] data, just in case the user wants to edit a fruit.
  onInit() {
    if (fruit != null) {
      _genusController.text = fruit!.genus;
      _familyController.text = fruit!.family;
      _nameController.text = fruit!.name;
      _carbohydratesController.text =
          fruit!.nutritions['carbohydrates'].toString();
      _proteinController.text = fruit!.nutritions['protein'].toString();
      _fatController.text = fruit!.nutritions['fat'].toString();
      _caloriesController.text = fruit!.nutritions['calories'].toString();
      _sugarController.text = fruit!.nutritions['sugar'].toString();
    }
  }

  /// Update [fruit] data with text controllers data, just in case the user wants to edit a fruit.
  updateModel() {
    fruit = Fruit(
      genus: _genusController.text,
      family: _familyController.text,
      name: _nameController.text,
      nutritions: {
        'carbohydrates': double.parse(_carbohydratesController.text),
        'protein': double.parse(_proteinController.text),
        'fat': double.parse(_fatController.text),
        'calories': double.parse(_caloriesController.text),
        'sugar': double.parse(_sugarController.text),
      },
    );
  }

  /// Clean all text controllers.
  cleanTextControllers() {
    _genusController.text = '';
    _familyController.text = '';
    _nameController.text = '';
    _carbohydratesController.text = '';
    _proteinController.text = '';
    _fatController.text = '';
    _caloriesController.text = '';
    _sugarController.text = '';
  }

  readBlocProvider(BuildContext context) {
    return context.read<CrudFruitBloc>();
  }

  /// Determine if the user wants to create or edit a fruit.
  ///
  /// If [fruit] is null, it means the user wants to create a fruit.
  /// If [fruit] is not null, it means the user wants to edit a fruit.
  handlerButtom(state, BuildContext context) async {
    final blocProvider = readBlocProvider(context);
    final userName = LoginBloc.getUser(context)!.name;
    if (fruit == null) {
      blocProvider.add(CreatedByChanged(
        createdBy: userName,
      ));
      blocProvider.add(CreateFruit(
        context: context,
      ));

      // Todo: back action
      showCustomSnackbar(
        title: 'Fruit added',
        message: 'Fruit added successfully',
        type: CustomSnackbarType.success,
      );
      cleanTextControllers();
    } else {
      blocProvider.add(
        UpdateFruit(
          context: context,
        ),
      );
      updateModel();
      // Todo: back action
      showCustomSnackbar(
        title: 'Fruit updated',
        message: 'Fruit updated successfully',
        type: CustomSnackbarType.success,
      );
    }
  }

  @override
  Widget build(BuildContext context) {
    onInit();
    return BlocBuilder<CrudFruitBloc, CrudFruitState>(
      builder: (context, state) {
        final blocProvider = readBlocProvider(context);
        return BlocBuilder<ResponsiveBloc, ResponsiveState>(
          builder: (context, state) {
            final theme = state.themeData!;
            return Form(
              key: _formKey,
              child: Column(
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  children: [
                    Row(
                      mainAxisAlignment: MainAxisAlignment.start,
                      children: [
                        Text(
                          "Basic Information",
                          style: theme.textTheme.headline2,
                        ),
                      ],
                    ),
                    const SizedBox(height: 10),
                    WidgetTextField(
                      active: fruit == null,
                      label: "Name",
                      controller: _nameController,
                      validator: (value) {
                        return validateText(value!);
                      },
                      onChanged: (value) {
                        blocProvider.add(
                          NameChanged(
                            name: value!,
                          ),
                        );
                      },
                    ),
                    WidgetTextField(
                        label: "Genus",
                        controller: _genusController,
                        validator: (value) {
                          return validateText(value!);
                        },
                        onChanged: (value) {
                          blocProvider.add(
                            GenusChanged(
                              genus: value!,
                            ),
                          );
                        }),
                    WidgetTextField(
                        label: "Family",
                        controller: _familyController,
                        validator: (value) {
                          return validateText(value!);
                        },
                        onChanged: (value) {
                          blocProvider.add(
                            FamilyChanged(
                              family: value!,
                            ),
                          );
                        }),
                    const Divider(),
                    Row(
                      children: [
                        Text(
                          "Nutritions:",
                          style: theme.textTheme.headline2,
                        ),
                      ],
                    ),
                    const SizedBox(height: 10),
                    WidgetTextField(
                        label: "Carbohydrates",
                        controller: _carbohydratesController,
                        numbersOnly: true,
                        validator: (value) {
                          return isNum(value!);
                        },
                        onChanged: (value) {
                          blocProvider.add(
                            NutritionsChanged(nutritions: {
                              "carbohydrates": double.parse(value!)
                            }),
                          );
                        }),
                    WidgetTextField(
                        label: "Protein",
                        controller: _proteinController,
                        numbersOnly: true,
                        validator: (value) {
                          return isNum(value!);
                        },
                        onChanged: (value) {
                          blocProvider.add(
                            NutritionsChanged(
                                nutritions: {"protein": double.parse(value!)}),
                          );
                        }),
                    WidgetTextField(
                        label: "Fat",
                        controller: _fatController,
                        numbersOnly: true,
                        validator: (value) {
                          return isNum(value!);
                        },
                        onChanged: (value) {
                          blocProvider.add(
                            NutritionsChanged(
                                nutritions: {"fat": double.parse(value!)}),
                          );
                        }),
                    WidgetTextField(
                        label: "Calories",
                        controller: _caloriesController,
                        numbersOnly: true,
                        validator: (value) {
                          return isNum(value!);
                        },
                        onChanged: (value) {
                          blocProvider.add(
                            NutritionsChanged(
                                nutritions: {"calories": double.parse(value!)}),
                          );
                        }),
                    WidgetTextField(
                        label: "Sugar",
                        controller: _sugarController,
                        numbersOnly: true,
                        validator: (value) {
                          return isNum(value!);
                        },
                        onChanged: (value) {
                          blocProvider.add(
                            NutritionsChanged(
                                nutritions: {"sugar": double.parse(value!)}),
                          );
                        }),
                    Row(
                      children: [
                        Expanded(
                          child: WidgetButton(
                              text: "Save",
                              onPressed: () async {
                                final form = _formKey.currentState;
                                form!.save();
                                if (form.validate()) {
                                  try {
                                    await handlerButtom(state, context);
                                  } catch (e) {
                                    showCustomSnackbar(
                                        title: "Error",
                                        message: e.toString(),
                                        type: CustomSnackbarType.error);
                                  }
                                }
                              },
                              typeMain: true),
                        ),
                      ],
                    ),
                  ]),
            );
          },
        );
      },
    );
  }
}
