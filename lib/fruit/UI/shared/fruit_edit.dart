import 'package:flutter/material.dart';
import 'package:flutter_api/fruit/UI/shared/custom_fruit_form.dart';
import 'package:flutter_api/fruit/model/fruit_model.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import '../../../bloc/responsive/responsive_bloc.dart';
import '../../bloc/fruit/fruit_bloc.dart';

// ignore: must_be_immutable
class FruitEditWidget extends StatelessWidget {
  /// Is a [StatelessWidget] that edit a fruit.
  ///
  /// This widget use an observable [Fruit] to fill the details.
  /// [fromPage] is the page where the fruit is shown.
  /// [fruit] is taken from the [FruitController] depending on [fromPage]
  /// If [fromPage] is [fruit_user_page] the fruit is taken from [fruitController.selectedFruitUser]
  /// If [fromPage] is [fruit_page] the fruit is taken from [fruitController.selectedFruit]
  FruitEditWidget({Key? key, this.fromPage = "fruit_page"}) : super(key: key);
  late Fruit fruit;
  String fromPage;

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<ResponsiveBloc, ResponsiveState>(
      builder: (context, state) {
        final theme = state.themeData!;
        return BlocBuilder<FruitBloc, FruitState>(
          builder: (context, state) {
            if (fromPage == "fruit_user_page") {
              fruit = state.selectedFruitUser!;
            } else {
              fruit = state.selectedFruit!;
            }

            return Container(
              margin: const EdgeInsets.only(top: 20, left: 20, right: 20),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  Text(
                    "Edit fruit",
                    style: theme.textTheme.headline1,
                  ),
                  const SizedBox(height: 30),
                  Expanded(
                    child: ListView(
                      controller: ScrollController(),
                      scrollDirection: Axis.vertical,
                      children: [
                        CustomFruitForm(fruit: fruit!),
                      ],
                    ),
                  ),
                ],
              ),
            );
          },
        );
      },
    );
  }
}
