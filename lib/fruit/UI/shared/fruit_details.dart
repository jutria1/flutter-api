import 'package:flutter/material.dart';
import 'package:flutter_api/bloc/responsive/responsive_bloc.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import '../../bloc/fruit/fruit_bloc.dart';
import '../../model/fruit_model.dart';

// ignore: must_be_immutable
class FruitDetailsWidget extends StatelessWidget {
  /// Is a [StatelessWidget] that show the details of a fruit.
  ///
  /// This widget use an observable [Fruit] to fill the details.
  /// [fromPage] is the page where the fruit is shown.
  /// [fruit] is taken from the [FruitController] depending on [fromPage]
  /// If [fromPage] is [fruit_user_page] the fruit is taken from [fruitController.selectedFruitUser]
  /// If [fromPage] is [fruit_page] the fruit is taken from [fruitController.selectedFruit]
  FruitDetailsWidget({Key? key, this.fromPage = "fruit_page"})
      : super(key: key);

  late Fruit fruit;
  String fromPage;

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<ResponsiveBloc, ResponsiveState>(
      builder: (context, state) {
        final theme = state.themeData!;
        final nutritionsStyle = theme.textTheme.bodyText1;
        return BlocBuilder<FruitBloc, FruitState>(
          builder: (context, state) {
            if (fromPage == "fruit_user_page") {
              fruit = state.selectedFruitUser!;
            } else {
              fruit = state.selectedFruit!;
            }
            if (fruit.name.isNotEmpty) {
              return Column(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                    children: [
                      Text("Name", style: theme.textTheme.headline2),
                      Text(fruit.name, style: theme.textTheme.headline3),
                    ],
                  ),
                  const SizedBox(height: 20),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                    children: [
                      Text("Genus", style: theme.textTheme.headline2),
                      Text(fruit.genus, style: theme.textTheme.headline3),
                    ],
                  ),
                  const SizedBox(height: 20),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                    children: [
                      Text("Family", style: theme.textTheme.headline2),
                      Text(fruit.family,
                          style: theme.textTheme.headline3),
                    ],
                  ),
                  const SizedBox(height: 20),
                  Column(
                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                    children: [
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                        children: [
                          Text("Nutritions", style: theme.textTheme.headline2),
                        ],
                      ),
                      const SizedBox(height: 10),
                      Text('Carbohydrates', style: theme.textTheme.headline3),
                      const SizedBox(height: 10),
                      Text(fruit.nutritions["carbohydrates"].toString(),
                          style: nutritionsStyle),
                      const SizedBox(height: 10),
                      Text('Protein', style: theme.textTheme.headline3),
                      const SizedBox(height: 10),
                      Text(fruit.nutritions["protein"].toString(),
                          style: nutritionsStyle),
                      const SizedBox(height: 10),
                      Text('Fat', style: theme.textTheme.headline3),
                      const SizedBox(height: 10),
                      Text(fruit.nutritions["fat"].toString(),
                          style: nutritionsStyle),
                      const SizedBox(height: 10),
                      Text('Calories', style: theme.textTheme.headline3),
                      const SizedBox(height: 10),
                      Text(fruit.nutritions["calories"].toString(),
                          style: nutritionsStyle),
                      const SizedBox(height: 10),
                      Text('Sugar', style: theme.textTheme.headline3),
                      const SizedBox(height: 10),
                      Text(fruit.nutritions["sugar"].toString(),
                          style: nutritionsStyle),
                    ],
                  ),
                ],
              );
            } else {
              return const Center(child: Text('No fruit selected'));
            }
          },
        );
      },
    );
  }
}
