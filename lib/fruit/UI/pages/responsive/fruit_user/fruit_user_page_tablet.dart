import 'package:flutter/material.dart';
import 'package:flutter_api/bloc/responsive/responsive_bloc.dart';
import 'package:flutter_api/fruit/UI/shared/custom_fruit_list_tile.dart';
import 'package:flutter_api/fruit/UI/shared/fruit_create.dart';
import 'package:flutter_api/fruit/UI/shared/fruit_edit.dart';
import 'package:flutter_api/fruit/bloc/crud_fruit/crud_fruit_bloc.dart';
import 'package:flutter_api/fruit/bloc/fruit/fruit_bloc.dart';
import 'package:flutter_api/user/blocs/auth/login/login_bloc.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import '../../../../../UI/shared/custom_snackbar.dart';
import '../../../shared/fruit_details.dart';

class FruitUserPageTablet extends StatelessWidget {
  /// Is a [StatelessWidget] that show fruit page in tablet.
  ///
  /// This widget use [Scaffold] and [CustomFruitListTile] to show the list of fruits.
  FruitUserPageTablet({Key? key}) : super(key: key);

  fruitBlocProvider(BuildContext context) {
    return context.read<FruitBloc>();
  }

  crudFruitBlocProvider(BuildContext context) {
    return context.read<FruitBloc>();
  }

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<ResponsiveBloc, ResponsiveState>(
      builder: (context, state) {
        final userName = LoginBloc.getUser(context)!.userName;
        final theme = state.themeData!;
        return Scaffold(
          body: Row(
            children: [
              BlocBuilder<FruitBloc, FruitState>(
                builder: (context, state) {
                  final fruitBloc = fruitBlocProvider(context);
                  final crudFruitBloc = crudFruitBlocProvider(context);

                  if (state.loadStatus == FruitLoadStatus.loading) {
                    return const Expanded(
                        child: Center(child: CircularProgressIndicator()));
                  } else {
                    final fruits = state.fruits
                        .where((fruit) => fruit.createdBy == userName)
                        .toList();
                    return Expanded(
                        child: ListView.separated(
                      controller: ScrollController(),
                      separatorBuilder: (context, index) => const Divider(),
                      scrollDirection: Axis.vertical,
                      itemCount: fruits.length,
                      itemBuilder: (context, index) {
                        final fruit = fruits.reversed.elementAt(index);
                        return CustomFruitListTile(
                            fruit: fruit,
                            onTap: () {
                              fruitBloc
                                  .add(FruitSelectedUserChanged(fruit: fruit));
                              fruitBloc.add(FruitIndexUserPageChanged(
                                  indexFruitUserPage: 0));
                            },
                            onEdit: () {
                              fruitBloc
                                  .add(FruitSelectedUserChanged(fruit: fruit));
                              fruitBloc.add(FruitIndexUserPageChanged(
                                  indexFruitUserPage: 1));
                            },
                            onDelete: () async {
                              crudFruitBloc.add(DeleteFruit(
                                  name: fruit.name, context: context));
                              showCustomSnackbar(
                                title: 'Fruit deleted',
                                message: 'Fruit deleted successfully',
                              );
                              fruitBloc
                                  .add(FruitSelectedUserChanged(fruit: null));
                              fruitBloc.add(FruitIndexUserPageChanged(
                                  indexFruitUserPage: 0));
                            });
                      },
                    ));
                  }
                },
              ),
              BlocBuilder<FruitBloc, FruitState>(builder: (context, state) {
                if (state.indexFruitPage == 0) {
                  return Expanded(
                      child: ListView(
                    controller: ScrollController(),
                    children: [
                      FruitDetailsWidget(fromPage: "fruit_user_page"),
                    ],
                  ));
                } else if (state.indexFruitPage == 1) {
                  return Expanded(
                      child: FruitEditWidget(fromPage: "fruit_user_page"));
                } else {
                  return const Expanded(child: FruitCreateWidget());
                }
              })
            ],
          ),
          floatingActionButton: FloatingActionButton(
            backgroundColor: theme.primaryColor,
            heroTag: "createFruit${DateTime.now()}",
            onPressed: () {
              fruitBlocProvider(context)
                  .add(FruitIndexUserPageChanged(indexFruitUserPage: 2));
            },
            child: const Icon(Icons.add),
          ),
        );
      },
    );
  }
}
