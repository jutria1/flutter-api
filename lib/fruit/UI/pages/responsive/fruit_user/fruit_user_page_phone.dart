import 'package:flutter/material.dart';
import 'package:flutter_api/UI/shared/custom_snackbar.dart';
import 'package:flutter_api/bloc/responsive/responsive_bloc.dart';
import 'package:flutter_api/fruit/UI/shared/custom_fruit_list_tile.dart';
import 'package:flutter_api/fruit/bloc/crud_fruit/crud_fruit_bloc.dart';
import 'package:flutter_api/fruit/bloc/fruit/fruit_bloc.dart';
import 'package:flutter_api/user/blocs/auth/login/login_bloc.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class FruitUserPagePhone extends StatelessWidget {
  /// Is a [StatelessWidget] that show user fruit page in phone.
  ///
  /// This widget use [Scaffold] and [CustomFruitListTile] to show the list of fruits.

  FruitUserPagePhone({Key? key}) : super(key: key);

  fruitBlocProvider(BuildContext context) {
    return context.read<FruitBloc>();
  }

  crudFruitBlocProvider(BuildContext context) {
    return context.read<FruitBloc>();
  }

  @override
  Widget build(BuildContext context) {
    final fruitBloc = fruitBlocProvider(context);
    final userName = LoginBloc.getUser(context)!.userName;
    return BlocBuilder<ResponsiveBloc, ResponsiveState>(
      builder: (context, state) {
        final theme = state.themeData!;
        return Scaffold(
          body: Column(
            children: [
              BlocListener<CrudFruitBloc, CrudFruitState>(
                listener: (context, state) {
                  // TODO: implement listener
                  // Todo Show a snackbar when the fruit is deleted.
                },
                child: BlocBuilder<FruitBloc, FruitState>(
                  builder: (context, state) {
                    if (state.loadStatus == FruitLoadStatus.loading) {
                      return const Center(child: CircularProgressIndicator());
                    } else {
                      final fruits = state.fruits
                          .where((fruit) => fruit.createdBy == userName)
                          .toList();
                      return Expanded(
                          child: ListView.separated(
                        controller: ScrollController(),
                        separatorBuilder: (context, index) => const Divider(),
                        scrollDirection: Axis.vertical,
                        itemCount: fruits.length,
                        itemBuilder: (context, index) {
                          final fruit = fruits.reversed.elementAt(index);

                          return CustomFruitListTile(
                            fruit: fruit,
                            onTap: () {
                              fruitBloc
                                  .add(FruitSelectedUserChanged(fruit: fruit));
                              // Todo: Show a page to show the fruit details. FruitDetailsPage()
                            },
                            onEdit: () {
                              fruitBloc
                                  .add(FruitSelectedUserChanged(fruit: fruit));
                              //Todo: Show a page to edit the fruit. FruitEditPage()
                              /*Future.delayed(
                                    const Duration(milliseconds: 500),
                                        () => Get.to(() => FruitEditPage()));*/
                            },
                            onDelete: () async {
                              crudFruitBlocProvider(context).add(DeleteFruit(
                                  context: context, name: fruit.name));
                            },
                          );
                        },
                      ));
                    }
                  },
                ),
              ),
            ],
          ),
          floatingActionButton: FloatingActionButton(
            backgroundColor: theme.primaryColor,
            heroTag: "createFruit${DateTime.now()}",
            onPressed: () {
              // Todo: show  FruitCreatePage()
            },
            child: const Icon(Icons.add),
          ),
        );
      },
    );
  }
}
