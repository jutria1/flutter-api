import 'package:flutter/material.dart';
import 'package:flutter_api/bloc/responsive/responsive_bloc.dart';
import 'package:flutter_bloc/flutter_bloc.dart';


import 'fruit_user_page_phone.dart';
import 'fruit_user_page_tablet.dart';

class ResponsiveFruitUserPage {
  /// Is a [GetResponsiveView] that show user fruit page in phone and tablet.
  ///
  /// If the device is tablet, this widget use [FruitUserPageTablet] to show the list of fruits.
  /// If the device is phone, this widget use [FruitUserPagePhone] to show the list of fruits.


  Widget builder() {
    return BlocBuilder<ResponsiveBloc, ResponsiveState>(
      builder: (context, state) {
        return state.typeScreen == TypeScreen.mobile
            ? FruitUserPagePhone()
            : FruitUserPageTablet();
      },
    );
  }
}
