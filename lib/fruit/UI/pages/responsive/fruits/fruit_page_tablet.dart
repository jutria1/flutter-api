import 'package:flutter/material.dart';
import 'package:flutter_api/bloc/responsive/responsive_bloc.dart';
import 'package:flutter_api/fruit/UI/shared/custom_fruit_list_tile.dart';
import 'package:flutter_api/fruit/UI/shared/fruit_create.dart';
import 'package:flutter_api/fruit/UI/shared/fruit_edit.dart';
import 'package:flutter_api/fruit/bloc/crud_fruit/crud_fruit_bloc.dart';
import 'package:flutter_api/fruit/bloc/fruit/fruit_bloc.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import '../../../../../UI/shared/custom_snackbar.dart';
import '../../../shared/fruit_details.dart';

class FruitPageTablet extends StatelessWidget {
  /// Is a [StatelessWidget] that show fruit page in tablet.
  ///
  /// This widget use [Scaffold] and [CustomFruitListTile] to show the list of fruits.
  FruitPageTablet({Key? key}) : super(key: key);

  fruitBlocProvider(BuildContext context) {
    return context.read<FruitBloc>();
  }

  crudFruitBlocProvider(BuildContext context) {
    return context.read<FruitBloc>();
  }

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<ResponsiveBloc, ResponsiveState>(
      builder: (context, state) {
        final theme = state.themeData!;
        return Scaffold(
          body: Row(
            children: [
              BlocBuilder<FruitBloc, FruitState>(
                builder: (context, state) {
                  final fruitBloc = fruitBlocProvider(context);
                  final crudFruitBloc = crudFruitBlocProvider(context);

                  if (state.loadStatus == FruitLoadStatus.loading) {
                    return const Expanded(
                        child: Center(child: CircularProgressIndicator()));
                  } else {
                    return Expanded(
                        child: ListView.separated(
                      controller: ScrollController(),
                      separatorBuilder: (context, index) => const Divider(),
                      scrollDirection: Axis.vertical,
                      itemCount: state.fruits.length,
                      itemBuilder: (context, index) {
                        final fruit = state.fruits.reversed.elementAt(index);
                        return CustomFruitListTile(
                            fruit: fruit,
                            onTap: () {
                              fruitBloc.add(FruitSelectedChanged(fruit: fruit));
                              fruitBloc.add(
                                  FruitIndexPageChanged(indexFruitPage: 0));
                            },
                            onEdit: () {
                              fruitBloc.add(FruitSelectedChanged(fruit: fruit));
                              fruitBloc.add(
                                  FruitIndexPageChanged(indexFruitPage: 1));
                            },
                            onDelete: () async {
                              crudFruitBloc.add(DeleteFruit(
                                  name: fruit.name, context: context));
                              showCustomSnackbar(
                                title: 'Fruit deleted',
                                message: 'Fruit deleted successfully',
                              );
                              fruitBloc.add(FruitSelectedChanged(fruit: null));
                              fruitBloc.add(
                                  FruitIndexPageChanged(indexFruitPage: 0));
                            });
                      },
                    ));
                  }
                },
              ),
              BlocBuilder<FruitBloc, FruitState>(builder: (context, state) {
                if (state.indexFruitPage == 0) {
                  return Expanded(
                      child: ListView(
                    controller: ScrollController(),
                    children: [
                      FruitDetailsWidget(),
                    ],
                  ));
                } else if (state.indexFruitPage == 1) {
                  return Expanded(child: FruitEditWidget());
                } else {
                  return const Expanded(child: FruitCreateWidget());
                }
              })
            ],
          ),
          floatingActionButton: FloatingActionButton(
            backgroundColor: theme.primaryColor,
            heroTag: "createFruit${DateTime.now()}",
            onPressed: () {
              fruitBlocProvider(context)
                  .add(FruitIndexPageChanged(indexFruitPage: 2));
            },
            child: const Icon(Icons.add),
          ),
        );
      },
    );
  }
}
