import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import '../../../../../bloc/responsive/responsive_bloc.dart';
import 'fruit_page_phone.dart';
import 'fruit_page_tablet.dart';

class ResponsiveFruitPage {
  /// Is a [GetResponsiveView] that show fruit page in phone and tablet.
  ///
  /// If the device is tablet, this widget use [FruitPageTablet] to show the list of fruits.
  /// If the device is phone, this widget use [FruitPagePhone] to show the list of fruits.


  Widget builder() {
    return BlocBuilder<ResponsiveBloc, ResponsiveState>(
      builder: (context, state) {
        return state.typeScreen == TypeScreen.mobile
            ? FruitPagePhone()
            : FruitPageTablet();
      },
    );
  }
}
