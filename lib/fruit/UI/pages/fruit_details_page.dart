import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import '../../../UI/shared/custom_app_bar.dart';
import '../../../bloc/responsive/responsive_bloc.dart';
import '../shared/fruit_details.dart';

class FruitDetailsPage extends StatelessWidget {
  /// Is a [StatelessWidget] that create or edit a fruit.
  ///
  /// This widget use [Scaffold] and [FruitDetailsWidget] to show the details of a fruit.
  FruitDetailsPage({Key? key, this.fromPage = "fruit_user"}) : super(key: key);

  String fromPage;

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<ResponsiveBloc, ResponsiveState>(
      builder: (context, state) {
        final theme = state.themeData!;
        return Scaffold(
            appBar: WidgetAppBarBack().build(context),
            body: Column(
              children: [
                Text("Fruit Details", style: theme.textTheme.headline1),
                Expanded(
                    child: ListView(
                  controller: ScrollController(),
                  children: [
                    FruitDetailsWidget(fromPage: fromPage),
                  ],
                )),
              ],
            ));
      },
    );
  }
}
