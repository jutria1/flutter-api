part of 'fruit_bloc.dart';

@immutable
abstract class FruitEvent {}

class LoadFruit extends FruitEvent {
  final FruitLoadStatus loadStatus;

  LoadFruit({required this.loadStatus});
}

class LoadUserFruit extends FruitEvent {
  final FruitLoadStatus loadStatusUser;

  LoadUserFruit({required this.loadStatusUser});
}

class FruitListChanged extends FruitEvent {
  final  Function callback;

  FruitListChanged({required this.callback});
}

class FruitSelectedChanged extends FruitEvent {
  final Fruit? fruit;

  FruitSelectedChanged({this.fruit});
}

class FruitIndexPageChanged extends FruitEvent {
  final int? indexFruitPage;

  FruitIndexPageChanged({this.indexFruitPage});
}

class FruitSelectedUserChanged extends FruitEvent {
  final Fruit? fruit;

  FruitSelectedUserChanged({this.fruit});
}

class FruitIndexUserPageChanged extends FruitEvent {
  final int? indexFruitUserPage;

  FruitIndexUserPageChanged({this.indexFruitUserPage});
}
