import 'dart:async';

import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import '../../model/fruit_model.dart';
import '../../repository/api_client.dart';
import '../../repository/storage.dart';

part 'fruit_event.dart';

part 'fruit_state.dart';

class FruitBloc extends Bloc<FruitEvent, FruitState> {
  FruitBloc() : super(FruitState());
  final StorageFruitRepository _fruitRepository = StorageFruitRepository();
  final FruitApiClient _fruitApiClient = FruitApiClient();

  static addFruitInListFruit(BuildContext context, Fruit fruit) async {
    final state = BlocProvider.of<FruitBloc>(context).state;
    BlocProvider.of<FruitBloc>(context).add(FruitListChanged(callback: () {
      final list = state.fruits;
      list.add(fruit);
      return list;
    }));
  }

  static updateFruitInListFruit(BuildContext context, Fruit fruit) async {
    final state = BlocProvider.of<FruitBloc>(context).state;
    BlocProvider.of<FruitBloc>(context).add(FruitListChanged(callback: () {
      final fruits = state.fruits;
      final index =
          fruits.indexWhere((element) => element.name == fruit.name);
      fruits[index] = fruit;
      return fruits;
    }));
  }

  static deleteFruitInListFruit(BuildContext context, String name) async {
    final state = BlocProvider.of<FruitBloc>(context).state;
    BlocProvider.of<FruitBloc>(context).add(FruitListChanged(callback: () {
      final fruits = state.fruits;
      fruits.removeWhere((element) => element.name == name);
      return fruits;
    }));
  }

  @override
  Stream<FruitState> mapEventToState(FruitEvent event) async* {
    if (event is LoadFruit) {
      if (event.loadStatus == FruitLoadStatus.initial) {
        yield state.copyWith(loadStatus: FruitLoadStatus.loading);
        try {
          final listFruitApi=await _fruitApiClient.getFruitsFromApi();
          final listFruitStored = await _fruitRepository.readAll();
          listFruitApi.addAll(listFruitStored!);
          yield state.copyWith(
            fruits: listFruitApi,
            loadStatus: FruitLoadStatus.loaded,
          );
        } catch (e) {
          yield state.copyWith(
            loadStatus: FruitLoadStatus.error,
            throwMessage: e.toString(),
          );
        }
      }
    } else if (event is LoadUserFruit) {
      if (event.loadStatusUser == FruitLoadStatus.initial) {
        yield state.copyWith(loadStatusUser: FruitLoadStatus.loading);
        try {
          final listFruit = await _fruitRepository.readAll();
          yield state.copyWith(
            fruits: listFruit,
            loadStatusUser: FruitLoadStatus.loaded,
          );
        } catch (e) {
          yield state.copyWith(
            loadStatusUser: FruitLoadStatus.error,
            throwMessage: e.toString(),
          );
        }
      }
    } else if (event is FruitSelectedChanged) {
      yield state.copyWith(selectedFruit: event.fruit);
    } else if (event is FruitSelectedUserChanged) {
      yield state.copyWith(selectedFruitUser: event.fruit);
    } else if (event is FruitIndexPageChanged) {
      yield state.copyWith(indexFruitPage: event.indexFruitPage);
    } else if (event is FruitIndexUserPageChanged) {
      yield state.copyWith(indexFruitUserPage: event.indexFruitUserPage);
    } else if (event is FruitListChanged) {
      final list = event.callback!();
      yield state.copyWith(fruits: list);
    }
  }
}
