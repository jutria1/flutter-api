part of 'fruit_bloc.dart';

enum FruitLoadStatus {
  loading,
  loaded,
  error,
  initial,
}

class FruitState {
  final FruitLoadStatus loadStatus;
  final String throwMessage;

  final FruitLoadStatus loadStatusUser;

  /// [indexFruitPage] is an int that is used to store the index of the fruit page.
  final List<Fruit> fruits;

  /// [selectedFruit] is used for fruit responsive pages.
  ///
  /// [selectedFruit] is a [Fruit] that is used to store the fruit that is selected.
  /// [selectedFruit] allows the user to see the fruit and update it.
  final Fruit? selectedFruit;

  /// [indexFruitPage] is used for fruit responsive pages.
  ///
  /// [indexFruitPage] is an int that is used to store the index of the fruit page.
  /// [indexFruitPage] allows the user to change the fruit page.
  final int? indexFruitPage;

  /// [selectedFruitUser] is used for user fruits responsive pages.
  ///
  /// [selectedFruitUser] is a [Fruit] that is used to store the fruit that is selected.
  /// In this case [selectedFruitUser] indicates what is the selected fruit in the user fruits page.
  final Fruit? selectedFruitUser;

  /// [indexFruitUserPage] is used for user fruits responsive pages.
  ///
  /// [indexFruitUserPage] is an int that is used to store the index of the user fruits page.
  /// [indexFruitUserPage] allows the user to change the user fruits page.
  final int? indexFruitUserPage;

  FruitState({
    this.fruits = const [],
    this.selectedFruit,
    this.indexFruitPage,
    this.selectedFruitUser,
    this.indexFruitUserPage,
    this.loadStatus = FruitLoadStatus.initial,
    this.loadStatusUser = FruitLoadStatus.initial,
    this.throwMessage = "",
  });

  FruitState copyWith({
    List<Fruit>? fruits,
    Fruit? selectedFruit,
    int? indexFruitPage,
    Fruit? selectedFruitUser,
    int? indexFruitUserPage,
    FruitLoadStatus? loadStatus,
    FruitLoadStatus? loadStatusUser,
    String? throwMessage,
  }) {
    return FruitState(
      fruits: fruits ?? this.fruits,
      selectedFruit: selectedFruit ?? this.selectedFruit,
      indexFruitPage: indexFruitPage ?? this.indexFruitPage,
      selectedFruitUser: selectedFruitUser ?? this.selectedFruitUser,
      indexFruitUserPage: indexFruitUserPage ?? this.indexFruitUserPage,
      loadStatus: loadStatus ?? this.loadStatus,
      loadStatusUser: loadStatusUser ?? this.loadStatusUser,
      throwMessage: throwMessage ?? this.throwMessage,
    );
  }
}
