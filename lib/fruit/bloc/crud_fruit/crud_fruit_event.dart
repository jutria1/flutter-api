part of 'crud_fruit_bloc.dart';

@immutable
abstract class CrudFruitEvent {}

class CreateFruit extends CrudFruitEvent {
  final BuildContext context;

  CreateFruit({required this.context});
}

class UpdateFruit extends CrudFruitEvent {
  final BuildContext context;

  UpdateFruit({required this.context});
}

class DeleteFruit extends CrudFruitEvent {
  final String name;
  final BuildContext context;

  DeleteFruit({required this.name, required this.context});
}

class GetFruit extends CrudFruitEvent {
  final String name;

  GetFruit({required this.name});
}

class GenusChanged extends CrudFruitEvent {
  final String genus;

  GenusChanged({required this.genus});
}

class FamilyChanged extends CrudFruitEvent {
  final String family;

  FamilyChanged({required this.family});
}

class NameChanged extends CrudFruitEvent {
  final String name;

  NameChanged({required this.name});
}

class NutritionsChanged extends CrudFruitEvent {
  final Map<String, dynamic> nutritions;

  NutritionsChanged({required this.nutritions});
}

class CreatedByChanged extends CrudFruitEvent {
  final String createdBy;

  CreatedByChanged({required this.createdBy});
}
