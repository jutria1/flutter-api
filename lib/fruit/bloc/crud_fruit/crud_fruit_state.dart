part of 'crud_fruit_bloc.dart';

enum FruitTransactionStatus {
  loading,
  loaded,
  error,
  initial,
}

class CrudFruitState {
  final String? genus;
  final String? family;
  final String? name;
  final Map<String, dynamic>? nutritions;
  final String? createdBy;
  final FruitTransactionStatus status;
  final String throwMessage;

  CrudFruitState(
      {this.genus,
      this.family,
      this.name,
      this.nutritions,
      this.createdBy,
      this.status = FruitTransactionStatus.initial,
      this.throwMessage = ""});

  CrudFruitState copyWith({
    String? genus,
    String? family,
    String? name,
    Map<String, dynamic>? nutritions,
    String? createdBy,
    FruitTransactionStatus? status,
    String? throwMessage,
  }) {
    return CrudFruitState(
      genus: genus ?? this.genus,
      family: family ?? this.family,
      name: name ?? this.name,
      nutritions: nutritions ?? this.nutritions,
      createdBy: createdBy ?? this.createdBy,
      status: status ?? this.status,
      throwMessage: throwMessage ?? this.throwMessage,
    );
  }
}
