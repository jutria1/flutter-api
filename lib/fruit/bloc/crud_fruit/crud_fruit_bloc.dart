import 'dart:async';

import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import '../../model/fruit_model.dart';
import '../../repository/storage.dart';
import '../fruit/fruit_bloc.dart';

part 'crud_fruit_event.dart';

part 'crud_fruit_state.dart';

class CrudFruitBloc extends Bloc<CrudFruitEvent, CrudFruitState> {
  CrudFruitBloc() : super(CrudFruitState());

  final StorageFruitRepository _fruitRepository = StorageFruitRepository();

  addFruitInList(CreateFruit event, Map<String, dynamic> map) {
    FruitBloc.addFruitInListFruit(
        event.context, Fruit.fromJsonString(map.toString()));
  }

  updateFruitInList(UpdateFruit event, Map<String, dynamic> map) {
    FruitBloc.updateFruitInListFruit(
        event.context, Fruit.fromJsonString(map.toString()));
  }

  deleteFruitInList(DeleteFruit event) {
    FruitBloc.deleteFruitInListFruit(event.context, event.name);
  }

  @override
  Stream<CrudFruitState> mapEventToState(CrudFruitEvent event) async* {
    if (event is GenusChanged) {
      yield state.copyWith(genus: event.genus);
    } else if (event is FamilyChanged) {
      yield state.copyWith(family: event.family);
    } else if (event is NameChanged) {
      yield state.copyWith(name: event.name);
    } else if (event is NutritionsChanged) {
      Map<String, dynamic> nutritions = event.nutritions;
      Map<String, dynamic> nutritionsCopy = Map.from(nutritions);
      nutritions.forEach((key, value) {
        nutritionsCopy[key] = value;
      });
      yield state.copyWith(nutritions: nutritionsCopy);
    } else if (event is CreatedByChanged) {
      yield state.copyWith(createdBy: event.createdBy);
    } else if (event is CreateFruit) {
      yield state.copyWith(status: FruitTransactionStatus.loading);
      try {
        final map = {
          'genus': state.genus,
          'family': state.family,
          'name': state.name,
          'nutritions': state.nutritions,
          'createdBy': state.createdBy,
        };
        await _fruitRepository.create(state.name!, map);
        addFruitInList(event, map);
        yield state.copyWith(status: FruitTransactionStatus.loaded);
        yield CrudFruitState();
      } catch (e) {
        yield state.copyWith(
            status: FruitTransactionStatus.error, throwMessage: e.toString());
      }
    } else if (event is UpdateFruit) {
      yield state.copyWith(status: FruitTransactionStatus.loading);
      try {
        final map = {
          'genus': state.genus,
          'family': state.family,
          'name': state.name,
          'nutritions': state.nutritions,
          'createdBy': state.createdBy,
        };
        await _fruitRepository.update(state.name!, map);
        yield state.copyWith(status: FruitTransactionStatus.loaded);
        updateFruitInList(event, map);
        yield CrudFruitState();
      } catch (e) {
        yield state.copyWith(
            status: FruitTransactionStatus.error, throwMessage: e.toString());
      }
    } else if (event is DeleteFruit) {
      yield state.copyWith(status: FruitTransactionStatus.loading);
      try {
        await _fruitRepository.delete(event.name);
        deleteFruitInList(event);
        yield CrudFruitState();
      } catch (e) {
        yield state.copyWith(
            status: FruitTransactionStatus.error, throwMessage: e.toString());
      }
    } else if (event is GetFruit) {
      try {
        final result = await _fruitRepository.readByName(event.name);
        final fruit = result!;
        yield state.copyWith(
          genus: fruit.genus,
          family: fruit.family,
          name: fruit.name,
          nutritions: fruit.nutritions,
          createdBy: fruit.createdBy,
        );
      } catch (e) {
        yield state.copyWith(
            status: FruitTransactionStatus.error, throwMessage: e.toString());
      }
    }
  }
}
