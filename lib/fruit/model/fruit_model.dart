import 'dart:convert';

class Fruit {
  String genus;
  String family;
  int? id;
  String name;
  Map<String, dynamic> nutritions;
  String? createdBy;

  /// Create a new fruit.
  ///
  /// The [id] is optional. If not provided, it will be generated.
  /// The [createdBy] is optional and is the user name who created the fruit.
  /// The [nutritions] is required and must be a map with the nutritions of the fruit.
  /// The [nutritions] must have the following keys:
  /// - calories
  /// - protein
  /// - fat
  /// - carbohydrates
  /// - sugar
  /// The [name] is required and must be a string.
  /// The [genus] is required and must be a string.
  ///  The [family] is required and must be a string.
  Fruit(
      {required this.genus,
      required this.family,
      this.id,
      required this.name,
      required this.nutritions,
      this.createdBy});

  factory Fruit.fromJsonString(String jsonString) {
    final map = json.decode(jsonString);
    try {
      return Fruit(
        genus: map['genus'],
        family: map['family'],
        id: map['id'],
        name: map['name'],
        nutritions: map['nutritions'],
        createdBy: map['createdBy'],
      );
    } catch (e) {
      return Fruit(
        genus: '',
        family: '',
        id: null,
        name: '',
        nutritions: {},
        createdBy: '',
      );
    }
  }

  String toJsonString() {
    final map = {
      'genus': genus,
      'family': family,
      'id': id,
      'name': name,
      'nutritions': nutritions,
      'createdBy': createdBy,
    };
    return map.toString();
  }

  Map<String, dynamic> toJson() {
    return {
      'genus': genus,
      'family': family,
      'id': id,
      'name': name,
      'nutritions': nutritions,
      'createdBy': createdBy,
    };
  }

  @override
  String toString() {
    return 'Fruit{genus: $genus, family: $family, id: $id, name: $name, nutritions: $nutritions, createdBy: $createdBy}';
  }
}
