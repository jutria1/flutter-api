part of 'responsive_bloc.dart';



class ResponsiveState {
  final TypeScreen typeScreen;
  late ThemeData? themeData;
  Color primaryColor = const Color.fromARGB(255, 251, 207, 150);
  Color secondaryColor = const Color.fromARGB(255, 136, 135, 130);
  Color thirdColor = const Color.fromARGB(255, 155, 155, 155);

  ResponsiveState({this.typeScreen = TypeScreen.mobile}) {
    if (typeScreen == TypeScreen.mobile) {
      themeData = mobileThemeData();
    } else {
      themeData = tabletThemeData();
    }
  }

  mobileThemeData() {
    const TextStyle defaultTextStyle = TextStyle(
      color: Colors.black,
      fontSize: 16,
    );
    final textTheme = themeData!.textTheme.copyWith(
      headline1: const TextStyle(
        fontSize: 30,
        fontWeight: FontWeight.bold,
        color: Color.fromARGB(255, 87, 83, 83),
      ),
      headline2: const TextStyle(
        fontSize: 20,
        fontWeight: FontWeight.bold,
        color: Colors.teal,
      ),
      headline3: TextStyle(
        fontSize: 18,
        fontWeight: FontWeight.bold,
        color: secondaryColor,
      ),
      subtitle1: TextStyle(
        fontSize: 16,
        color: thirdColor,
      ),
      subtitle2: defaultTextStyle,
      bodyText1: const TextStyle(
        fontSize: 18,
        color: Color.fromARGB(255, 87, 83, 83),
      ),
    );
    return themeData?.copyWith(
        textTheme: textTheme,
        primaryTextTheme: textTheme,
        primaryColor: primaryColor,
        primaryColorLight: primaryColor,
        appBarTheme: AppBarTheme(
          color: primaryColor,
          iconTheme:
              const IconThemeData(color: Color.fromARGB(255, 251, 207, 150)),
        ));
  }

  tabletThemeData() {
    const TextStyle defaultTextStyle = TextStyle(
      color: Colors.black,
      fontSize: 16,
    );
    final textTheme = themeData!.textTheme.copyWith(
      headline1: const TextStyle(
        fontSize: 40,
        fontWeight: FontWeight.bold,
        color: Color.fromARGB(255, 87, 83, 83),
      ),
      headline2: const TextStyle(
        fontSize: 30,
        fontWeight: FontWeight.bold,
        color: Colors.teal,
      ),
      headline3: TextStyle(
        fontSize: 25,
        fontWeight: FontWeight.bold,
        color: secondaryColor,
      ),
      subtitle1: TextStyle(
        fontSize: 20,
        color: thirdColor,
      ),
      subtitle2: defaultTextStyle,
      bodyText1: const TextStyle(
        fontSize: 22,
        color: Color.fromARGB(255, 87, 83, 83),
      ),
    );
    return themeData!.copyWith(
        textTheme: textTheme,
        primaryTextTheme: textTheme,
        primaryColor: primaryColor,
        primaryColorLight: primaryColor,
        appBarTheme: AppBarTheme(
          color: primaryColor,
          iconTheme:
              const IconThemeData(color: Color.fromARGB(255, 251, 207, 150)),
        ));
  }
}
