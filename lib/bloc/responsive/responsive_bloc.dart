import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:flutter/material.dart';
import 'package:meta/meta.dart';

part 'responsive_event.dart';

part 'responsive_state.dart';

enum TypeScreen { mobile, tablet }

class ResponsiveBloc extends Bloc<ResponsiveEvent, ResponsiveState> {
  ResponsiveBloc() : super(ResponsiveState());

  @override
  Stream<ResponsiveState> mapEventToState(ResponsiveEvent event) async* {
    if (event is ChangeScreen) {
      yield ResponsiveState(typeScreen: event.typeScreen);
    }
  }
}
