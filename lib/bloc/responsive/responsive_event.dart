part of 'responsive_bloc.dart';

@immutable
abstract class ResponsiveEvent {}

class ChangeScreen extends ResponsiveEvent {
  final TypeScreen typeScreen;

  ChangeScreen({this.typeScreen = TypeScreen.mobile});
}
