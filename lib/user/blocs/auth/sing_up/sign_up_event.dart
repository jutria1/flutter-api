part of 'sign_up_bloc.dart';

@immutable
abstract class SignUpEvent {}

class SignUpNameChanged extends SignUpEvent {
  final String? name;

  SignUpNameChanged({this.name});
}

class SignUpUserNameChanged extends SignUpEvent {
  final String? userName;

  SignUpUserNameChanged({this.userName});
}

class SignUpLastNameChanged extends SignUpEvent {
  final String? lastName;

  SignUpLastNameChanged({this.lastName});
}

class SignUpEmailChanged extends SignUpEvent {
  final String? email;

  SignUpEmailChanged({this.email});
}

class SignUpPasswordChanged extends SignUpEvent {
  final String? password;

  SignUpPasswordChanged({this.password});
}

class SignUpDescriptionChanged extends SignUpEvent {
  final String? description;

  SignUpDescriptionChanged({this.description});
}

class SignUpUrlImageChanged extends SignUpEvent {
  final String? urlImage;

  SignUpUrlImageChanged({this.urlImage});
}

class SignUpSubmitted extends SignUpEvent {
  final BuildContext context;
  final User? user;

  SignUpSubmitted({required this.context, this.user});
}
