import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:flutter/material.dart';
import 'package:flutter_api/config/validations.dart';
import 'package:flutter_api/user/model/user_model.dart';
import 'package:flutter_api/user/repository/auth.dart';
import 'package:meta/meta.dart';

part 'sign_up_event.dart';

part 'sign_up_state.dart';

enum FormSubmissionStatus { initial, submitting, success, error }

class SignUpBloc extends Bloc<SignUpEvent, SignUpState> {
  AuthenticationRepository? authenticationRepository =
      AuthenticationRepository();

  SignUpBloc() : super(SignUpState());

  @override
  Stream<SignUpState> mapEventToState(SignUpEvent event) async* {
    if (event is SignUpNameChanged) {
      yield state.copyWith(name: event.name);
    } else if (event is SignUpLastNameChanged) {
      yield state.copyWith(lastName: event.lastName);
    } else if (event is SignUpEmailChanged) {
      yield state.copyWith(email: event.email);
    } else if (event is SignUpPasswordChanged) {
      yield state.copyWith(password: event.password);
    } else if (event is SignUpDescriptionChanged) {
      yield state.copyWith(description: event.description);
    } else if (event is SignUpUrlImageChanged) {
      yield state.copyWith(urlImage: event.urlImage);
    } else if (event is SignUpSubmitted) {
      yield state.copyWith(formStatus: FormSubmissionStatus.submitting);
      try {
        if (event.user == null) {
          await authenticationRepository!.registerUser(state.userName, {
            'name': state.name,
            'lastName': state.lastName,
            'email': state.email,
            'password': state.password,
            'description': state.description,
            'urlImage': state.urlImage,
            'userName': state.userName,
          });
          yield state.copyWith(
            formStatus: FormSubmissionStatus.success,
          );
        } else {
          await authenticationRepository!.updateUser(event.user, {
            'name': state.name,
            'lastName': state.lastName,
            'email': state.email,
            'password': state.password,
            'description': state.description,
            'urlImage': state.urlImage,
            'userName': state.userName,
          });
          yield state.copyWith(
            formStatus: FormSubmissionStatus.success,
          );
        }
      } catch (e) {
        yield state.copyWith(
            exceptionThrown: e.toString(),
            formStatus: FormSubmissionStatus.error);
      }
    }
  }
}
