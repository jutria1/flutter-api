part of 'sign_up_bloc.dart';



class SignUpState {
  final String userName;

  String? get isValidUserName => validateText(userName);

  final String name;

  String? get isValidName => validateText(name);

  final String lastName;

  String? get isValidLastName => validateText(lastName);

  final String email;

  String? get isValidEmail => validateEmail(email);

  final String password;

  String? get isValidPassword => validatePassword(password);

  final String description;

  String? get isValidDescription => description.isNotEmpty ? null : "Description is required";

  final String urlImage;

  String? get isValidUrlImage => validateUrl(urlImage);

  final String exceptionThrown;
  final FormSubmissionStatus formStatus;

  SignUpState(
      {this.name = "",
      this.userName = "",
      this.lastName = "",
      this.email = "",
      this.password = "",
      this.description = "",
      this.urlImage = "",
      this.formStatus = FormSubmissionStatus.initial,
      this.exceptionThrown = 'A error has occurred'});

  SignUpState copyWith(
      {String? name,
      String? userName,
      String? lastName,
      String? email,
      String? password,
      String? description,
      String? urlImage,
      FormSubmissionStatus? formStatus,
      String? exceptionThrown}) {
    return SignUpState(
      name: name ?? this.name,
      lastName: lastName ?? this.lastName,
      email: email ?? this.email,
      password: password ?? this.password,
      description: description ?? this.description,
      urlImage: urlImage ?? this.urlImage,
      formStatus: formStatus ?? this.formStatus,
      exceptionThrown: exceptionThrown ?? this.exceptionThrown,
      userName: userName ?? this.userName,
    );
  }
}
