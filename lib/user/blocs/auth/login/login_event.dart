part of 'login_bloc.dart';

@immutable
abstract class LoginEvent {}

class LoginUserNameChanged extends LoginEvent {
  final String? username;
  LoginUserNameChanged({
    this.username
  });
}

class LoginPasswordChanged extends LoginEvent {
  final String? password;
  LoginPasswordChanged({
    this.password
  });
}

class LoginSubmitted extends LoginEvent {
  final BuildContext context;
  LoginSubmitted({required this.context});
}

class UserChanged extends LoginEvent {
  final User? user;
  UserChanged({required this.user});
}
