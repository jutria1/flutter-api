import 'dart:async';

import 'package:flutter/material.dart';
import 'package:flutter_api/user/model/user_model.dart';
import 'package:flutter_api/user/repository/auth.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import '../../../../config/validations.dart';

part 'login_event.dart';

part 'login_state.dart';

enum FormSubmissionStatus { initial, submitting, success, error }

class LoginBloc extends Bloc<LoginEvent, LoginState> {
  AuthenticationRepository? loginRepository = AuthenticationRepository();

  LoginBloc({this.loginRepository}) : super(LoginState());

  static User? getUser(BuildContext context) {
    return BlocProvider.of<LoginBloc>(context).state.user;
  }

  @override
  Stream<LoginState> mapEventToState(LoginEvent event) async* {
    if (event is LoginUserNameChanged) {
      yield state.copyWith(userName: event.username);
    } else if (event is LoginPasswordChanged) {
      yield state.copyWith(password: event.password);
    } else if (event is LoginSubmitted) {
      try {
        yield state.copyWith(formStatus: FormSubmissionStatus.submitting);
        final user =
            await loginRepository!.loginUser(state.userName, state.password);
        yield state.copyWith(
            formStatus: FormSubmissionStatus.success, user: user);
      } catch (e) {
        yield state.copyWith(
            exceptionThrown: e.toString(),
            formStatus: FormSubmissionStatus.error);
      }
    }
  }
}
