part of 'login_bloc.dart';



class LoginState {
  final String userName;
  final String password;
  final String exceptionThrown;
  final FormSubmissionStatus formStatus;
  final User? user;

  String? get isValidUsername => validateUserName(userName);

  String? get isValidPassword => validatePassword(password);

  LoginState({
    this.userName = '',
    this.password = '',
    this.formStatus = FormSubmissionStatus.initial,
    this.exceptionThrown = 'A error has occurred',
    this.user,
  });

  LoginState copyWith({
    String? userName,
    String? password,
    FormSubmissionStatus? formStatus,
    String? exceptionThrown,
    User? user,
  }) {
    return LoginState(
      userName: userName ?? this.userName,
      password: password ?? this.password,
      formStatus: formStatus?? this.formStatus,
      exceptionThrown: exceptionThrown ?? this.exceptionThrown,
      user: user ?? this.user,
    );
  }
}
