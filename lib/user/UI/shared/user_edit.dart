import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import '../../../bloc/responsive/responsive_bloc.dart';
import '../../blocs/auth/login/login_bloc.dart';
import 'auth/sign_up/sign_up_form.dart';

class UserEditWidget extends StatelessWidget {
  /// Is a [StatelessWidget] that edit an user.
  ///
  /// This widget use an observable [User] to fill the fields.
  /// [user] is taken from the [UserController.user]
  UserEditWidget({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<ResponsiveBloc, ResponsiveState>(
      builder: (context, state) {
        final theme = state.themeData!;
        return Container(
          margin: const EdgeInsets.only(top: 20, left: 20, right: 20),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              Text(
                "Edit user",
                style: theme.textTheme.headline1,
              ),
              const SizedBox(height: 30),
              Expanded(
                  child: ListView(
                controller: ScrollController(),
                scrollDirection: Axis.vertical,
                children: [
                  UserForm(user: BlocProvider.of<LoginBloc>(context).state.user)
                ],
              )),
            ],
          ),
        );
      },
    );
  }
}
