import 'package:flutter/material.dart';
import 'package:flutter_api/UI/shared/custom_buttom.dart';
import 'package:flutter_api/UI/shared/custom_snackbar.dart';
import 'package:flutter_api/UI/shared/custom_text_field.dart';
import 'package:flutter_api/user/blocs/auth/sing_up/sign_up_bloc.dart';
import 'package:flutter_api/user/model/user_model.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class UserForm extends StatelessWidget {
  UserForm({Key? key, this.user}) : super(key: key);
  final _formKey = GlobalKey<FormState>();
  final User? user;

  final _nameController = TextEditingController();
  final _lastNameController = TextEditingController();
  final _userNameController = TextEditingController();
  final _passwordController = TextEditingController();
  final _emailController = TextEditingController();
  final _urlImageController = TextEditingController();
  final _descriptionController = TextEditingController();
  final _confirmPasswordController = TextEditingController();

  /// Update text controllers with [user] data, just in case the user wants to edit an user.
  onInit() {
    if (user != null) {
      _nameController.text = user!.name;
      _lastNameController.text = user!.lastName;
      _userNameController.text = user!.userName;
      _passwordController.text = user!.password;
      _emailController.text = user!.email;
      _urlImageController.text = user!.urlImage;
      _descriptionController.text = user!.description;
    }
  }

  /// Determine if the user wants to create or edit an [User].
  ///
  /// If [user] is null, it means the user wants to create an [User].
  /// If [user] is not null, it means the user wants to edit an [User].
  handlerButton(BuildContext context) async {
    BlocProvider.of<SignUpBloc>(context)
        .add(SignUpSubmitted(context: context, user: user));
  }

  @override
  Widget build(BuildContext context) {
    onInit();
    return BlocListener<SignUpBloc, SignUpState>(
      listener: (context, state) {
        if (state.formStatus == FormSubmissionStatus.success) {
          // Todo: back action
          if (user == null) {
            showCustomSnackbar(
                title: "Succes", message: "User added successfully");
          } else {
            showCustomSnackbar(
                title: "Succes", message: "User updated successfully");
          }
        } else if (state.formStatus == FormSubmissionStatus.error) {
          showCustomSnackbar(title: "Error", message: state.exceptionThrown);
        }
      },
      child: BlocBuilder<SignUpBloc, SignUpState>(
        builder: (context, state) {
          return Form(
            key: _formKey,
            child: Column(children: [
              WidgetTextField(
                label: "Name",
                controller: _nameController,
                validator: (value) {
                  return state.isValidName;
                },
                onChanged: (value) {
                  context
                      .read<SignUpBloc>()
                      .add(SignUpNameChanged(name: value));
                },
              ),
              WidgetTextField(
                  label: "Last Name",
                  controller: _lastNameController,
                  validator: (value) {
                    return state.isValidLastName;
                  },
                  onChanged: (value) {
                    context
                        .read<SignUpBloc>()
                        .add(SignUpLastNameChanged(lastName: value));
                  }),
              WidgetTextField(
                  active: user == null,
                  label: "User Name",
                  controller: _userNameController,
                  validator: (value) {
                    return state.isValidUserName;
                  },
                  onChanged: (value) {
                    context
                        .read<SignUpBloc>()
                        .add(SignUpUserNameChanged(userName: value));
                  }),
              WidgetTextField(
                  label: "Password",
                  controller: _passwordController,
                  obscure: true,
                  validator: (value) {
                    return state.isValidPassword;
                  },
                  onChanged: (value) {
                    context
                        .read<SignUpBloc>()
                        .add(SignUpPasswordChanged(password: value));
                  }),
              WidgetTextField(
                  label: "Confirm Password",
                  controller: _confirmPasswordController,
                  obscure: true,
                  validator: (value) {
                    if (value!.isEmpty) {
                      return "Confirm Password is required";
                    }
                    if (value != _passwordController.text) {
                      return "Password does not match";
                    }
                    return null;
                  }),
              WidgetTextField(
                  label: "Email",
                  controller: _emailController,
                  validator: (value) {
                    return state.isValidEmail;
                  },
                  onChanged: (value) {
                    context
                        .read<SignUpBloc>()
                        .add(SignUpEmailChanged(email: value));
                  }),
              WidgetTextField(
                  label: "Url Image",
                  controller: _urlImageController,
                  validator: (value) {
                    return state.isValidUrlImage;
                  },
                  onChanged: (value) {
                    context
                        .read<SignUpBloc>()
                        .add(SignUpUrlImageChanged(urlImage: value));
                  }),
              WidgetTextField(
                  label: "Description",
                  controller: _descriptionController,
                  validator: (value) {
                    return state.isValidDescription;
                  },
                  onChanged: (value) {
                    context
                        .read<SignUpBloc>()
                        .add(SignUpDescriptionChanged(description: value));
                  }),
              WidgetButton(
                  text: "Save",
                  onPressed: () async {
                    final form = _formKey.currentState;
                    form!.save();

                    if (form.validate()) {
                      try {
                        await handlerButton(context);
                      } catch (e) {
                        showCustomSnackbar(
                            title: "Error", message: e.toString());
                      }
                    }
                  },
                  typeMain: true)
            ]),
          );
        },
      ),
    );
  }
}
