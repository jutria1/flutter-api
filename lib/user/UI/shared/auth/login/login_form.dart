import 'package:flutter/material.dart';
import 'package:flutter_api/UI/shared/custom_buttom.dart';
import 'package:flutter_api/UI/shared/custom_text_field.dart';
import 'package:flutter_api/bloc/responsive/responsive_bloc.dart';
import 'package:flutter_api/user/blocs/auth/login/login_bloc.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import '../../../../../UI/shared/custom_app_bar.dart';

class LoginPage extends StatelessWidget {
  /// Is a [StatelessWidget] that login an user.
  ///
  /// this widget use [UserController] to login an user.
  /// [LoginPage] constains all text fields to login an user.
  LoginPage({Key? key}) : super(key: key);
  final _formKey = GlobalKey<FormState>();
  final _userNameController = TextEditingController();
  final _passwordController = TextEditingController();

  handlerButton(BuildContext context) {
    BlocProvider.of<LoginBloc>(context).add(LoginSubmitted(context: context));
  }

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<ResponsiveBloc, ResponsiveState>(
      builder: (context, state) {
        ThemeData theme = state.themeData!;
        return BlocListener<LoginBloc, LoginState>(
          listener: (context, state) {
            // TODO: implement listener
          },
          child: BlocBuilder<LoginBloc, LoginState>(
            builder: (context, state) {
              return Scaffold(
                appBar: WidgetAppBarBack().build(context),
                body: Container(
                  margin: const EdgeInsets.only(top: 20, left: 20, right: 20),
                  child: Form(
                    key: _formKey,
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: [
                        Text(
                          "Login",
                          style: theme.textTheme.headline1,
                        ),
                        const SizedBox(height: 30),
                        WidgetTextField(
                            label: "UserName",
                            controller: _userNameController,
                            onChanged: (value) {
                              BlocProvider.of<LoginBloc>(context)
                                  .add(LoginUserNameChanged(username: value));
                            },
                            validator: (value) {
                              return state.isValidUsername;
                            }),
                        WidgetTextField(
                            label: "Password",
                            controller: _passwordController,
                            obscure: true,
                            onChanged: (value) {
                              BlocProvider.of<LoginBloc>(context)
                                  .add(LoginPasswordChanged(password: value));
                            },
                            validator: (value) {
                              return state.isValidPassword;
                            }),
                        WidgetButton(
                            text: "Login",
                            onPressed: () async {
                              final form = _formKey.currentState;
                              form!.save();
                              if (form.validate()) {
                                handlerButton(context);
                              }
                            },
                            typeMain: true)
                      ],
                    ),
                  ),
                ),
              );
            },
          ),
        );
      },
    );
  }
}
