import 'package:flutter/material.dart';
import 'package:flutter_api/bloc/responsive/responsive_bloc.dart';
import 'package:flutter_api/fruit/bloc/fruit/fruit_bloc.dart';

import 'package:flutter_bloc/flutter_bloc.dart';

import '../../blocs/auth/login/login_bloc.dart';

class UserProfileWidget extends StatelessWidget {
  /// Is a [StatelessWidget] that show the user profile.
  ///
  /// This widget use an observable [User] to fill the details.

  UserProfileWidget({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<ResponsiveBloc, ResponsiveState>(
      builder: (context, state) {
        final theme = state.themeData!;
        return BlocBuilder<LoginBloc, LoginState>(
          builder: (context, state) {
            return state.user != null
                ? Center(
                    child: Container(
                    margin: const EdgeInsets.only(top: 20, left: 30, right: 30),
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Text("${state.user!.userName} Profile",
                            style: theme.textTheme.headline1),
                        ClipOval(
                          child: SizedBox.fromSize(
                            size: const Size.fromRadius(150),
                            child: Image.network(
                              state.user!.urlImage,
                              fit: BoxFit.cover,
                              errorBuilder: (context, error, stackTrace) =>
                                  const Icon(
                                Icons.person_rounded,
                                color: Colors.grey,
                                size: 150,
                              ),
                            ),
                          ),
                        ),
                        const SizedBox(height: 10),
                        Text("Description", style: theme.textTheme.headline2),
                        const SizedBox(height: 10),
                        Text(state.user!.description,
                            style: TextStyle(
                              fontSize: theme.textTheme.subtitle1!.fontSize,
                              color: theme.textTheme.subtitle1!.color,
                              overflow: TextOverflow.ellipsis,
                            )),
                        const SizedBox(height: 10),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            Text("Name", style: theme.textTheme.headline2),
                            Text("${state.user!.name} ${state.user!.lastName}",
                                style: theme.textTheme.headline3),
                          ],
                        ),
                        const SizedBox(height: 10),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            Text("Email", style: theme.textTheme.headline2),
                            Text(state.user!.email,
                                style: theme.textTheme.headline3),
                          ],
                        ),
                        const SizedBox(height: 10),
                        Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              Text("Created Fruits",
                                  style: theme.textTheme.headline2),
                              Text(
                                  BlocProvider.of<FruitBloc>(context)
                                      .state
                                      .fruits
                                      .where((element) =>
                                          element.createdBy ==
                                          state.user!.userName)
                                      .length
                                      .toString(),
                                  style: theme.textTheme.headline3),
                            ])
                      ],
                    ),
                  ))
                : Container();
          },
        );
      },
    );
  }
}
