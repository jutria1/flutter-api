import 'package:flutter/material.dart';
import 'package:flutter_api/UI/shared/custom_app_bar.dart';
import 'package:flutter_api/bloc/responsive/responsive_bloc.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import '../shared/auth/sign_up/sign_up_form.dart';

class SignUpPage extends StatelessWidget {
  /// Is a [StatelessWidget] that sign up an user.
  ///
  /// this widget use [Scaffold] and [UserForm] to sign up an user.
  /// [SignUpPage] constains all text fields to sign up an user.
  SignUpPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<ResponsiveBloc, ResponsiveState>(
      builder: (context, state) {
        final theme = state.themeData!;
        return Scaffold(
          appBar: WidgetAppBarBack().build(context),
          body: Container(
            margin: const EdgeInsets.only(top: 20, left: 20, right: 20),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                Text(
                  "Sign Up",
                  style: theme.textTheme.headline1,
                ),
                const SizedBox(height: 30),
                const SizedBox(height: 20),
                Expanded(
                  child: ListView(
                    controller: ScrollController(),
                    scrollDirection: Axis.vertical,
                    children: [
                      UserForm(),
                    ],
                  ),
                ),
              ],
            ),
          ),
        );
      },
    );
  }
}
