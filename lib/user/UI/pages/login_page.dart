import 'package:flutter/material.dart';
import 'package:flutter_api/UI/shared/custom_buttom.dart';
import 'package:flutter_api/UI/shared/custom_snackbar.dart';
import 'package:flutter_api/UI/shared/custom_text_field.dart';
import 'package:flutter_api/bloc/responsive/responsive_bloc.dart';
import 'package:flutter_api/config/validations.dart';
import 'package:flutter_api/fruit/bloc/fruit/fruit_bloc.dart';
import 'package:flutter_api/user/blocs/auth/login/login_bloc.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import '../../../UI/shared/custom_app_bar.dart';

class LoginPage extends StatelessWidget {
  /// Is a [StatelessWidget] that login an user.
  ///
  /// this widget use [UserController] to login an user.
  /// [LoginPage] constains all text fields to login an user.
  LoginPage({Key? key}) : super(key: key);
  final _formKey = GlobalKey<FormState>();
  final _userNameController = TextEditingController();
  final _passwordController = TextEditingController();

  fruitBlocProvider(BuildContext context) =>
      BlocProvider.of<FruitBloc>(context);

  loginBlocProvider(BuildContext context) =>
      BlocProvider.of<LoginBloc>(context);

  @override
  Widget build(BuildContext context) {
    final loginBloc = loginBlocProvider(context);
    final fruitBloc = fruitBlocProvider(context);
    return BlocBuilder<ResponsiveBloc, ResponsiveState>(
      builder: (context, state) {
        final theme = state.themeData!;
        return Scaffold(
          appBar: WidgetAppBarBack().build(context),
          body: Container(
            margin: const EdgeInsets.only(top: 20, left: 20, right: 20),
            child: Form(
              key: _formKey,
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  Text(
                    "Login",
                    style: theme.textTheme.headline1,
                  ),
                  const SizedBox(height: 30),
                  WidgetTextField(
                      label: "UserName",
                      controller: _userNameController,
                      onChanged: (value) {
                        loginBloc.add(LoginUserNameChanged(username: value));
                      },
                      validator: (value) {
                        return validateUserName(value!);
                      }),
                  WidgetTextField(
                      label: "Password",
                      controller: _passwordController,
                      obscure: true,
                      validator: (value) {
                        if (value!.isEmpty) return "Password is required";
                        return null;
                      },
                      onChanged: (value) {
                        loginBloc.add(LoginPasswordChanged(password: value));
                      }),
                  WidgetButton(
                      text: "Login",
                      onPressed: () async {
                        final form = _formKey.currentState;
                        form!.save();
                        if (form.validate()) {
                          try {
                            await loginBloc.login(
                                userName: _userNameController.text,
                                password: _passwordController.text);
                            fruitBloc.add(
                                LoadFruit(loadStatus: FruitLoadStatus.initial));
                            // todo: navigate to home page
                          } catch (e) {
                            showCustomSnackbar(
                              title: "Error",
                              message: e.toString(),
                              type: CustomSnackbarType.error,
                            );
                          }
                        }
                      },
                      typeMain: true)
                ],
              ),
            ),
          ),
        );
      },
    );
  }
}
