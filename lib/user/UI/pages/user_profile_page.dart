import 'package:flutter/material.dart';

import '../shared/user_profile.dart';

class UserProfilePage extends StatelessWidget {
  /// Is a [StatelessWidget] that show an user.
  ///
  /// this widget use [Scaffold] and [UserProfileWidget] to show an user.
  UserProfilePage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: ListView(
        controller: ScrollController(),
        children: [
          UserProfileWidget(),
        ],
      ),
    );
  }
}
