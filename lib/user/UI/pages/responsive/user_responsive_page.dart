import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import '../../../../bloc/responsive/responsive_bloc.dart';
import '../user_profile_page.dart';
import 'user_page_tablet.dart';

class ResponsiveUserPage {
  /// Is a [GetResponsiveView] that show user page in phone and tablet.
  ///
  /// If the device is tablet, this widget use [UserProfileTablet] to show the user page.
  /// If the device is phone, this widget use [UserProfilePage] to show the user page.

  Widget builder() => BlocBuilder<ResponsiveBloc, ResponsiveState>(
        builder: (context, state) {
          return state.typeScreen == TypeScreen.mobile
              ? UserProfilePage()
              : UserProfileTablet();
        },
      );
}
