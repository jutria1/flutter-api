import 'package:flutter/material.dart';
import 'package:flutter_api/user/UI/shared/user_edit.dart';
import 'package:flutter_api/user/UI/shared/user_profile.dart';


class UserProfileTablet extends StatelessWidget {
  /// Is a [StatelessWidget] that show user profile page in tablet.
  ///
  /// This widget use [Scaffold] and [UserProfileTablet] to show user profile.
  UserProfileTablet({Key? key}) : super(key: key);


  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
        child: Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Expanded(
                child: ListView(
              children: [
                UserProfileWidget(),
              ],
            )),
            Expanded(child: UserEditWidget())
          ],
        ),
      ),
    );
  }
}
