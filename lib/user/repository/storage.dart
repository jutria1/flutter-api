import 'dart:convert';

import 'package:flutter_api/repository/local_preferences.dart';
import 'package:flutter_api/user/model/user_model.dart';

class StorageUserRepository {

  StorageUserRepository() {
    _storage = LocalPreferences();
  }

  late LocalPreferences _storage;

  init() async {
    final users = await _storage.read('users');
    if (users == null) {
      final user = <String, dynamic>{
        "name": "Johan",
        "lastName": "Hurtado",
        "userName": "Johanl",
        "password": "123",
        "email": "admin@admin.com",
        "urlImage":
        "https://img.freepik.com/vector-premium/perfil-hombre-dibujos-animados_18591-58482.jpg?w=2000",
        "description": "Software developer",
        "createdFruits": 0
      };
      await _storage.save('users', [user.toString()]);
    }
  }


  Future<User> readUserLogged() async {
    final userName = await _storage.read('userLogged');
    final user = await readUserByUserName(userName);
    return user;
  }

  Future<void> saveUserLogged(String userName) async {
    await _storage.save('userLogged', userName);
  }

  Future<void> deleteUserLogged() async {
    await _storage.delete('userLogged');
  }


  Future<void> createUser(String userName, Map<String, dynamic> data) async {
    List<String> stringUsers = await _storage.read("users");
    final users = stringUsers.map<User?>((stringUser) =>
        User.fromJsonString(stringUser)).toList();
    final user = users.firstWhere((user) => user!.userName == userName,
        orElse: () => null);
    if (user == null) {
      stringUsers.add(data.toString());
      await _storage.save("users", users);
    } else {
      return Future.error('User already exists');
    }
  }

  Future<User> readUserByUserName(String userName) async {
    List<String> stringUsers = await _storage.read("users");
    final users = stringUsers.map<User?>((stringUser) =>
        User.fromJsonString(stringUser));
    final user = users.firstWhere((user) => user!.userName == userName,
        orElse: () => null);
    if (user != null) {
      return user;
    } else {
      return Future.error('User does not exists');
    }
  }

  Future<void> updateUser(String userName, Map<String, dynamic> data) async {
    List<String> stringUsers = await _storage.read("users");
    final mapUsers = stringUsers.map<Map<String, dynamic>>((stringUser) =>
        json.decode(stringUser)).toList();
    final user = mapUsers.firstWhere((user) => user[userName] == userName,
        orElse: () => {});
    if (user.isNotEmpty) {
      final index = mapUsers.indexOf(user);
      for (final key in data.keys) {
        user[key] = data[key] ?? user[key];
      }
      stringUsers[index] = user.toString();
      await _storage.save("users", stringUsers);
    } else {
      return Future.error('User does not exists');
    }
  }
}