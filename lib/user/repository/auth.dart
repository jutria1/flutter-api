import 'storage.dart';

class AuthenticationRepository {
  AuthenticationRepository() {
    _storage = StorageUserRepository();
  }

  late StorageUserRepository _storage;

  loginUser(username, password) async {
    try {
      final user = await _storage.readUserByUserName(username);
      if (user.password == password) {
        _storage.saveUserLogged(username);
        return user;
      } else {
        return Future.error('Password is incorrect');
      }
    } catch (e) {
      return Future.error(e);
    }
  }

  logoutUser() async {
    _storage.deleteUserLogged();
  }

  registerUser(username, data) async {
    try {
      final user = await _storage.readUserByUserName(username);
      await _storage.createUser(username, data);
      return user;
    } catch (e) {
      return Future.error(e);
    }
  }

  updateUser(username, data) async {
    try {
      final user = await _storage.readUserByUserName(username);
      await _storage.updateUser(username, data);
      return user;
    } catch (e) {
      return Future.error(e);
    }
  }
}
