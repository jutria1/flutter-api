import 'dart:convert';

class User {
  String name;
  String lastName;
  String userName;
  String password;
  String email;
  String urlImage;
  String description;

  /// [User] is used to create a new user.
  ///
  /// [name] is the name of the user.
  /// [lastName] is the last name of the user.
  /// [userName] is the user name of the user.
  /// [password] is the password of the user.
  /// [email] is the email of the user.
  /// [urlImage] is the url of the image of the user.
  /// [description] is the description of the user.
  User({
    required this.name,
    required this.lastName,
    required this.userName,
    required this.password,
    required this.email,
    this.urlImage = "",
    this.description = "",
  });

  factory User.fromJsonString(String jsonString) {
    final map=json.decode(jsonString);
    return User(
      name: map['name'],
      lastName: map['lastName'],
      userName: map['userName'],
      password: map['password'],
      email: map['email'],
      urlImage: map['urlImage'],
      description: map['description'],
    );
  }


  String toJsonString() {
    final map={
      'name': name,
      'lastName': lastName,
      'userName': userName,
      'password': password,
      'email': email,
      'urlImage': urlImage,
      'description': description,
    };
    return map.toString();
  }

  @override
  String toString() {
    return 'User{name: $name, lastName: $lastName, userName: $userName, password: $password, email: $email, urlImage: $urlImage, description: $description}';
  }
}
