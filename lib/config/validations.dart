String? validateUserName(String username) {
  if (username.isEmpty) {
    return "user name is required";
  } else if (username.contains(" ")) {
    return "user name must not contain spaces";
  }
  return null;
}

String? validatePassword(String password) {
  if (password.isEmpty) {
    return "password is required";
  }
  return null;
}

String? validateText(String text) {
  String pattern = r'(^[A-Za-zÀ-ÿ ]*$)';
  RegExp regExp = RegExp(pattern);
  if (!regExp.hasMatch(text)) {
    return "Text only contains letters";
  }
  return null;
}

String? validateEmail(String value) {
  String pattern =
      r'^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$';
  RegExp regExp = new RegExp(pattern);
  if (value.isEmpty) {
    return "Email is required";
  } else if (!regExp.hasMatch(value)) {
    return "Email is invalid";
  } else {
    return null;
  }
}

String? isNum(String value) {
  if (value.isEmpty) {
    return "Number is required";
  }
  try {
    double.parse(value);
  } catch (e) {
    return "Number is invalid";
  }
  return null;
}

String? validateUrl(String url) {
  if (Uri.parse(url).isAbsolute == false) {
    return "Url is invalid";
  }
  return null;
}
