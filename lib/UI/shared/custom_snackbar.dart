import 'package:flutter/material.dart';

/// [CustomSnackbar] is a type of [SnackBar] that show a message in the bottom of the screen.
enum CustomSnackbarType { success, error, info }

/// Select icon for [CustomSnackbarType]
///
/// [CustomSnackbarType.success] returns [Icons.check]
/// [CustomSnackbarType.error] returns [Icons.error]
/// [CustomSnackbarType.info] returns [Icons.info]
Icon getIcon(CustomSnackbarType type) {
  switch (type) {
    case CustomSnackbarType.success:
      return Icon(
        Icons.check_circle,
        color: Colors.green[700],
        size: 30,
      );
    case CustomSnackbarType.error:
      return Icon(
        Icons.error,
        color: Colors.red[700],
        size: 30,
      );
    case CustomSnackbarType.info:
      return Icon(
        Icons.info,
        color: Colors.blue[700],
        size: 30,
      );
    default:
      return Icon(
        Icons.info,
        color: Colors.blue[700],
        size: 30,
      );
  }
}

void showCustomSnackbar(
    {required String title,
    required String message,
    CustomSnackbarType type = CustomSnackbarType.success,
    Duration duration = const Duration(seconds: 3),
    bool? showProgressIndicator,
    }) {
  final snackBar = SnackBar(
    content: Column(
      children: [
        ListTile(
          leading: getIcon(type),
          title: Text(
            title,
            style: const TextStyle(
              fontSize: 20,
              fontWeight: FontWeight.bold,
            ),
          ),
          subtitle: Text(
            message,
            style: const TextStyle(
              fontSize: 15,
            ),
          ),
        ),
        if (showProgressIndicator == true) const LinearProgressIndicator()
      ],
    ),
    backgroundColor: Colors.grey[200],
    margin: const EdgeInsets.all(16),
    padding: const EdgeInsets.symmetric(
      horizontal: 20,
      vertical: 16,
    ),
    duration: duration,
    shape: RoundedRectangleBorder(
      borderRadius: BorderRadius.circular(20),
    ),
    dismissDirection: DismissDirection.horizontal,
    elevation: 10,
  );
  // todo: add ScaffoldMessaging
}
